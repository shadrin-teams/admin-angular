var mainApp =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('mainApp').controller('AppController', function ($scope, $state, $rootScope, $log, AuthService, AUTH_EVENTS) {

        $log.info('!', AuthService.isAuthenticated());
        $scope.userAuth = AuthService.isAuthenticated();

        $scope.toggle = function (id) {
            $(id).toggle(200);
        };

        $scope.logout = function () {
            $rootScope.$broadcast('logout', {});
            AuthService.logout();
            $state.go('login');
        };

        $scope.$on("login", function (event) {
            $log.info('login');
            $scope.userAuth = true;
        });

        $scope.$on("logout", function (event) {
            $log.info('logout');
            $scope.userAuth = false;
        });
    });
})();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp', ['ui.router', 'dataGrid', 'pagination', 'angularFileUpload', 'ngAnimate', 'toastr', 'froala', 'ngQuickDate']).config(function ($stateProvider, $urlRouterProvider) {

  $stateProvider.state('index', {
    url: '/',
    templateUrl: 'app/views/index.html',
    controller: 'IndexController'
  }).state('login', {
    url: '/login',
    templateUrl: 'app/views/login.html',
    controller: 'LoginController'
  }).state('sites', {
    url: '/sites',
    templateUrl: 'app/views/sites.html',
    controller: 'SitesController',
    controllerAs: 'sitesCtrl'
  }).state('site', {
    url: '/site/:id',
    templateUrl: 'app/views/site.html',
    controller: 'SiteController',
    controllerAs: 'siteCtrl'
  }).state('section', {
    url: '/section/:id/:sid',
    templateUrl: 'app/views/section.html',
    controller: 'SectionController',
    controllerAs: '$ctrl'
  });

  $urlRouterProvider.otherwise('/login');
}).run(function ($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
  $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
    if (!AuthService.isAuthenticated()) {
      if (next.name !== 'login' && next.name !== 'register') {
        event.preventDefault();
        $state.go('login');
      }
    }
  });

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    if (!angular.isString(error)) {
      error = JSON.stringify(error);
    }
    $log.error('$stateChangeError: ' + error);
  });
});

__webpack_require__(2);

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//ngQuickDate https://github.com/adamalbrecht/ngQuickDate
__webpack_require__(3);

// Controllers
__webpack_require__(0);
__webpack_require__(0);
__webpack_require__(0);
__webpack_require__(4);
__webpack_require__(5);
__webpack_require__(18);
__webpack_require__(22);
__webpack_require__(24);

// Views
__webpack_require__(6);
__webpack_require__(7);
__webpack_require__(8);
__webpack_require__(9);
__webpack_require__(20);
__webpack_require__(23);
__webpack_require__(25);

// Services
__webpack_require__(15);
__webpack_require__(16);

// Constants
__webpack_require__(17);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function(){var a;a=angular.module("ngQuickDate",[]),a.provider("ngQuickDateDefaults",function(){return{options:{dateFormat:"M/d/yyyy",timeFormat:"h:mm a",labelFormat:null,placeholder:"Click to Set Date",hoverText:null,buttonIconHtml:null,closeButtonHtml:"&times;",nextLinkHtml:"Next &rarr;",prevLinkHtml:"&larr; Prev",disableTimepicker:!1,disableClearButton:!1,defaultTime:null,dayAbbreviations:["Su","M","Tu","W","Th","F","Sa"],dateFilter:null,parseDateFunction:function(a){var b;return b=Date.parse(a),isNaN(b)?null:new Date(b)}},$get:function(){return this.options},set:function(a,b){var c,d,e;if("object"==typeof a){e=[];for(c in a)d=a[c],e.push(this.options[c]=d);return e}return this.options[a]=b}}}),a.directive("quickDatepicker",["ngQuickDateDefaults","$filter","$sce",function(a,b,c){return{restrict:"E",require:"?ngModel",scope:{dateFilter:"=?",onChange:"&",required:"@"},replace:!0,link:function(d,e,f,g){var h,i,j,k,l,m,n,o,p,q,r,s,t,u;return n=function(){var a;return r(),d.toggleCalendar(!1),d.weeks=[],d.inputDate=null,d.inputTime=null,d.invalid=!0,"string"==typeof f.initValue&&g.$setViewValue(f.initValue),d.defaultTime||(a=new Date(2013,0,1,12,0),d.datePlaceholder=b("date")(a,d.dateFormat),d.timePlaceholder=b("date")(a,d.timeFormat)),q(),p()},r=function(){var b,e;for(b in a)e=a[b],b.match(/[Hh]tml/)?d[b]=c.trustAsHtml(a[b]||""):!d[b]&&f[b]?d[b]=f[b]:d[b]||(d[b]=a[b]);return d.labelFormat||(d.labelFormat=d.dateFormat,d.disableTimepicker||(d.labelFormat+=" "+d.timeFormat)),f.iconClass&&f.iconClass.length?d.buttonIconHtml=c.trustAsHtml("<i ng-show='iconClass' class='"+f.iconClass+"'></i>"):void 0},i=!1,window.document.addEventListener("click",function(a){return d.calendarShown&&!i&&(d.toggleCalendar(!1),d.$apply()),i=!1}),angular.element(e[0])[0].addEventListener("click",function(a){return i=!0}),p=function(){var a;return a=g.$modelValue?o(g.$modelValue):null,t(),s(a),d.mainButtonStr=a?b("date")(a,d.labelFormat):d.placeholder,d.invalid=g.$invalid},s=function(a){return null!=a?(d.inputDate=b("date")(a,d.dateFormat),d.inputTime=b("date")(a,d.timeFormat)):(d.inputDate=null,d.inputTime=null)},q=function(a){var b;return null==a&&(a=null),b=null!=a?new Date(a):new Date,"Invalid Date"===b.toString()&&(b=new Date),b.setDate(1),d.calendarDate=new Date(b)},t=function(){var a,b,c,e,f,h,i,k,l,n,o,p,q,r;for(h=d.calendarDate.getDay(),e=m(d.calendarDate.getFullYear(),d.calendarDate.getMonth()),f=Math.ceil((h+e)/7),o=[],a=new Date(d.calendarDate),a.setDate(a.getDate()+-1*h),i=p=0,r=f-1;r>=0?r>=p:p>=r;i=r>=0?++p:--p)for(o.push([]),c=q=0;6>=q;c=++q)b=new Date(a),d.defaultTime&&(l=d.defaultTime.split(":"),b.setHours(l[0]||0),b.setMinutes(l[1]||0),b.setSeconds(l[2]||0)),k=g.$modelValue&&b&&j(b,g.$modelValue),n=j(b,new Date),o[i].push({date:b,selected:k,disabled:"function"==typeof d.dateFilter?!d.dateFilter(b):!1,other:b.getMonth()!==d.calendarDate.getMonth(),today:n}),a.setDate(a.getDate()+1);return d.weeks=o},g.$parsers.push(function(a){return d.required&&null==a?(g.$setValidity("required",!1),null):angular.isDate(a)?(g.$setValidity("required",!0),a):angular.isString(a)?(g.$setValidity("required",!0),d.parseDateFunction(a)):null}),g.$formatters.push(function(a){return angular.isDate(a)?a:angular.isString(a)?d.parseDateFunction(a):void 0}),h=function(a,c){return b("date")(a,c)},u=function(a){return"string"==typeof a?o(a):a},o=a.parseDateFunction,j=function(a,b,c){return null==c&&(c=!1),c?a-b===0:(a=u(a),b=u(b),a&&b&&a.getYear()===b.getYear()&&a.getMonth()===b.getMonth()&&a.getDate()===b.getDate())},k=function(a,b){return a&&b?parseInt(a.getTime()/6e4)===parseInt(b.getTime()/6e4):!1},m=function(a,b){return[31,a%4===0&&a%100!==0||a%400===0?29:28,31,30,31,30,31,31,30,31,30,31][b]},l=function(a,b){var c,d,e,f,g,h;return g=c=d=h=f=null,e=function(){var a;return a=+new Date-h,g=b>a&&a>0?setTimeout(e,b-a):null},function(){return d=this,c=arguments,h=+new Date,g||(g=setTimeout(e,b),f=a.apply(d,c),d=c=null),f}},g.$render=function(){return q(g.$viewValue),p()},g.$viewChangeListeners.unshift(function(){return q(g.$viewValue),p(),d.onChange?d.onChange():void 0}),d.$watch("calendarShown",function(a,b){var c;return a?(c=angular.element(e[0].querySelector(".quickdate-date-input"))[0],c.select()):void 0}),d.toggleCalendar=l(function(a){return isFinite(a)?d.calendarShown=a:d.calendarShown=!d.calendarShown},150),d.selectDate=function(a,b){var c;return null==b&&(b=!0),c=!g.$viewValue&&a||g.$viewValue&&!a||a&&g.$viewValue&&a.getTime()!==g.$viewValue.getTime(),"function"!=typeof d.dateFilter||d.dateFilter(a)?(g.$setViewValue(a),b&&d.toggleCalendar(!1),!0):!1},d.selectDateFromInput=function(a){var b,c,e,f;null==a&&(a=!1);try{if(c=o(d.inputDate),!c)throw"Invalid Date";if(!d.disableTimepicker&&d.inputTime&&d.inputTime.length&&c){if(f=d.disableTimepicker?"00:00:00":d.inputTime,e=o(""+d.inputDate+" "+f),!e)throw"Invalid Time";c=e}if(!k(g.$viewValue,c)&&!d.selectDate(c,!1))throw"Invalid Date";return a&&d.toggleCalendar(!1),d.inputDateErr=!1,d.inputTimeErr=!1}catch(h){if(b=h,"Invalid Date"===b)return d.inputDateErr=!0;if("Invalid Time"===b)return d.inputTimeErr=!0}},d.onDateInputTab=function(){return d.disableTimepicker&&d.toggleCalendar(!1),!0},d.onTimeInputTab=function(){return d.toggleCalendar(!1),!0},d.nextMonth=function(){return q(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()+1))),p()},d.prevMonth=function(){return q(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()-1))),p()},d.clear=function(){return d.selectDate(null,!0)},n()},template:"<div class='quickdate'>\n  <a href='' ng-focus='toggleCalendar()' class='quickdate-button' title='{{hoverText}}'><div ng-hide='iconClass' ng-bind-html='buttonIconHtml'></div>{{mainButtonStr}}</a>\n  <div class='quickdate-popup' ng-class='{open: calendarShown}'>\n    <a href='' tabindex='-1' class='quickdate-close' ng-click='toggleCalendar()'><div ng-bind-html='closeButtonHtml'></div></a>\n    <div class='quickdate-text-inputs'>\n      <div class='quickdate-input-wrapper'>\n        <label>Date</label>\n        <input class='quickdate-date-input' ng-class=\"{'ng-invalid': inputDateErr}\" name='inputDate' type='text' ng-model='inputDate' placeholder='{{ datePlaceholder }}' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onDateInputTab()' />\n      </div>\n      <div class='quickdate-input-wrapper' ng-hide='disableTimepicker'>\n        <label>Time</label>\n        <input class='quickdate-time-input' ng-class=\"{'ng-invalid': inputTimeErr}\" name='inputTime' type='text' ng-model='inputTime' placeholder='{{ timePlaceholder }}' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onTimeInputTab()'>\n      </div>\n    </div>\n    <div class='quickdate-calendar-header'>\n      <a href='' class='quickdate-prev-month quickdate-action-link' tabindex='-1' ng-click='prevMonth()'><div ng-bind-html='prevLinkHtml'></div></a>\n      <span class='quickdate-month'>{{calendarDate | date:'MMMM yyyy'}}</span>\n      <a href='' class='quickdate-next-month quickdate-action-link' ng-click='nextMonth()' tabindex='-1' ><div ng-bind-html='nextLinkHtml'></div></a>\n    </div>\n    <table class='quickdate-calendar'>\n      <thead>\n        <tr>\n          <th ng-repeat='day in dayAbbreviations'>{{day}}</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr ng-repeat='week in weeks'>\n          <td ng-mousedown='selectDate(day.date, true, true)' ng-click='$event.preventDefault()' ng-class='{\"other-month\": day.other, \"disabled-date\": day.disabled, \"selected\": day.selected, \"is-today\": day.today}' ng-repeat='day in week'>{{day.date | date:'d'}}</td>\n        </tr>\n      </tbody>\n    </table>\n    <div class='quickdate-popup-footer'>\n      <a href='' class='quickdate-clear' tabindex='-1' ng-hide='disableClearButton' ng-click='clear()'>Clear</a>\n    </div>\n  </div>\n</div>"}}]),a.directive("ngEnter",function(){return function(a,b,c){return b.bind("keydown keypress",function(b){return 13===b.which?(a.$apply(c.ngEnter),b.preventDefault()):void 0})}}),a.directive("onTab",function(){return{restrict:"A",link:function(a,b,c){return b.bind("keydown keypress",function(b){return 9!==b.which||b.shiftKey?void 0:a.$apply(c.onTab)})}}})}).call(this);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('IndexController', IndexController);
    function IndexController($scope, $rootScope, $http, $window, AuthService, API_ENDPOINT, $state) {
        $scope.destroySession = function () {
            AuthService.logout();
        };

        $scope.getInfo = function () {

            $http.get(API_ENDPOINT.url + '/post').then(function (result) {
                $scope.memberinfo = result.data.msg;
            });
        };
    };
})();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * AppController
     * Description: Sets up a controller
     */
    .controller('LoginController', LoginController);
    function LoginController($scope, AuthService, $rootScope, $state) {
        $scope.user = {
            name: '',
            password: ''
        };

        $scope.login = function () {
            AuthService.login($scope.user).then(function (msg) {
                $rootScope.$broadcast('login', { user: $scope.user });
                $state.go('index');
            }, function (errMsg) {
                alert('Login failed!' + errMsg);
            });
        };
    };
})();

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = "<h1>Angular adapter for the Froala WYSIWYG editor</h1>\r\n<div class=\"sample\">\r\n    <h2>Sample 1: Inline Edit</h2>\r\n    <div froala=\"titleOptions\" ng-model=\"myTitle\"></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 2: Full Editor</h2>\r\n    <textarea id=\"froala-sample-2\" froala=\"titleOptions2\" ng-model=\"sample2Text\"></textarea>\r\n    <h4>Rendered Content:</h4>\r\n    <div froala-view=\"sample2Text\"></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 3: Manual Initialization</h2>\r\n    <button class=\"manual\" ng-click=\"initControls.initialize()\">Initialize Editor</button>\r\n    <button ng-click=\"initControls.destroy()\" ng-show=\"initControls.getEditor() != null\">Close Editor</button>\r\n    <button ng-click=\"deleteAll()\" ng-show=\"initControls.getEditor() != null\">Delete All</button>\r\n    <div id=\"froala-sample-3\" froala froala-init=\"initialize(initControls)\" ng-model=\"sample3Text\">Check out the <a href=\"https://www.froala.com/wysiwyg-editor\">Froala Editor</a></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 4: Editor on 'img' tag</h2>\r\n    <img froala ng-model=\"imgModel\"/>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{imgModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 5: Editor on 'button' tag</h2>\r\n    <button froala ng-model=\"buttonModel\"></button>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{buttonModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 6: Editor on 'input' tag</h2>\r\n    <input froala=\"inputOptions\" ng-model=\"inputModel\"/>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{inputModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 7: Editor on 'a' tag. Manual Initialization</h2>\r\n    <button class=\"manual\" ng-click=\"linkInitControls.initialize()\">Initialize Editor</button>\r\n    <button ng-click=\"linkInitControls.destroy()\" ng-show=\"linkInitControls.getEditor() != null\">Close Editor</button>\r\n    <div>\r\n        <a froala froala-init=\"initializeLink(initControls)\" ng-model=\"linkModel\">Froala Editor</a>\r\n    </div>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{linkModel}}</div>\r\n</div>";

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row tile_count animate bounceInDown \">\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Категорий</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Товаров</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\">0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-clock-o\"></i> Просмотров</span>\r\n        <div class=\"count green\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Заказов</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"red\"><i class=\"fa fa-sort-desc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Обновлений</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Вы успешно авторизовались</h3></div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = "<div class=\"login_wrapper\">\r\n    <div class=\"animate form login_form wow bounceInLeft\">\r\n        <section class=\"login_content\">\r\n            <form>\r\n                <h1>Аторизация</h1>\r\n                <div>\r\n                    <input type=\"text\" class=\"form-control\"  ng-model=\"user.email\" placeholder=\"Email\" required=\"\" />\r\n                </div>\r\n                <div>\r\n                    <input type=\"password\" class=\"form-control\" ng-model=\"user.password\" placeholder=\"Password\" required=\"\" />\r\n                </div>\r\n                <div>\r\n                    <a class=\"btn btn-default submit\" href=\"#\" ng-click=\"login()\">Log in</a>\r\n                </div>\r\n\r\n                <div class=\"clearfix\"></div>\r\n            </form>\r\n        </section>\r\n    </div>\r\n</div>";

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = "<div class=\"menu_section\">\r\n    <h3>Основное</h3>\r\n    <ul class=\"nav side-menu\">\r\n        <li><a ui-sref=\"index\"><i class=\"fa fa-edit\"></i> Первая</a></li>\r\n        <li><a ui-sref=\"sites\"><i class=\"fa fa-edit\"></i> Сайты </a></li>\r\n    </ul>\r\n</div>";

/***/ }),
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').service('AuthService', function ($q, $http, API_ENDPOINT) {
    var LOCAL_TOKEN_KEY = 'token';
    var authenticated = false;
    var authToken = void 0;

    function loadUserCredentials() {
        var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
        if (token) {
            useCredentials(token);
            console.log("loadUserCredentials");
        }
    }

    function storeUserCredentials(token) {
        window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
        useCredentials(token);
    }

    function useCredentials(token) {
        authenticated = true;
        authToken = token;
        console.log('authenticated', authenticated);

        // Set the token as header for your requests!
        $http.defaults.headers.common.Authorization = 'JWT ' + authToken;
    }

    function destroyUserCredentials() {
        authToken = undefined;
        authenticated = false;
        $http.defaults.headers.common.Authorization = undefined;
        window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    }

    var register = function register(user) {
        return $q(function (resolve, reject) {
            $http.post(API_ENDPOINT.url + '/signup', user).then(function (result) {
                if (result.data.success) {
                    resolve(result.data.msg);
                } else {
                    reject(result.data.msg);
                }
            });
        });
    };

    var login = function login(user) {
        return $q(function (resolve, reject) {
            $http.post(API_ENDPOINT.url + '/auth', { email: user.email, password: user.password }).then(function (result) {
                if (result.data.success) {

                    storeUserCredentials(result.data.token);
                    resolve(result.data.msg);
                } else {
                    reject(result.data.msg);
                }
            });
        });
    };

    var logout = function logout() {
        destroyUserCredentials();
    };

    loadUserCredentials();

    return {
        login: login,
        register: register,
        logout: logout,
        isAuthenticated: function isAuthenticated() {
            return authenticated;
        }
    };
}).factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function responseError(response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated
            }[response.status], response);
            return $q.reject(response);
        }
    };
}).config(function ($httpProvider) {
    console.log('httpProvider', $httpProvider.interceptors);
    $httpProvider.interceptors.push('AuthInterceptor');
});

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').service('RestService', function ($q, $http, API_ENDPOINT) {

    function send(method, url, data) {

        return $http({
            method: method,
            url: API_ENDPOINT.url + '/' + url,
            data: data
        });
    }

    function get(url, data) {
        return send('GET', url, data);
    }

    function put(url, data) {
        return send('PUT', url, data);
    }

    function post(url, data) {
        return send('POST', url, data);
    }

    function del(url, data) {
        return send('DELETE', url, data);
    }

    return {
        post: post,
        put: put,
        get: get,
        delete: del
    };
});

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated'
}).constant('API_PUBLIC', {
  url: 'http://localhost:3001/'
}).constant('API_ENDPOINT', {
  url: 'http://localhost:3001/api' //188.120.241.78
});

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('SitesController', SitesController);
  function SitesController($scope, $log, $http, RestService, API_PUBLIC, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.$watch(function () {
      return $state.params.cat;
    }, function (newVal, oldVal) {
      $log.log('$watch $state', newVal);
    });

    $scope.listSite = [];
    $scope.cat = $state.params.cat;
    $scope.name = 'Посты';
    $scope.find = {};
    $scope.gridActions = {};
    $scope.publicUrl = API_PUBLIC.url;

    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('site/' + id).then(function (response) {

          if (response.data.status === 'deleted') {
            toastr.error('Продукция успешно удаленна');
            loadData();
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    function loadData() {
      RestService.get('site', {}).then(function (response) {
        $('table.table').show(400);
        $scope.listSite = response.data.data;
        $scope.gridOptions.data = response.data.data;
        console.log('get products', response);
      });
    };
  };
})();

/***/ }),
/* 19 */,
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Посты</h3></div>\r\n            <div class=\"form-group animate bounceInLeft text-right\">\r\n                <button ui-sref=\"site\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i> Добавить\r\n                </button>\r\n            </div>\r\n            <br/>\r\n\r\n            <div grid-data id='grid1' grid-options=\"gridOptions\" grid-actions=\"gridActions\">\r\n                <form class=\"form-inline pull-right margin-bottom-basic\">\r\n                    <div class=\"form-group\">\r\n                        <grid-pagination max-size=\"10\"\r\n                                         boundary-links=\"true\"\r\n                                         class=\"pagination-sm\"\r\n                                         total-items=\"paginationOptions.totalItems\"\r\n                                         ng-model=\"paginationOptions.currentPage\"\r\n                                         ng-change=\"reloadGrid()\"\r\n                                         items-per-page=\"paginationOptions.itemsPerPage\"></grid-pagination>\r\n                    </div>\r\n                    <div class=\"form-group items-per-page\">\r\n                        <label for=\"itemsOnPageSelect2\">Items per page:</label>\r\n                        <select id=\"itemsOnPageSelect2\" class=\"form-control input-sm\"\r\n                                ng-init=\"paginationOptions.itemsPerPage = '10'\"\r\n                                ng-model=\"paginationOptions.itemsPerPage\" ng-change=\"reloadGrid()\">\r\n                            <option>10</option>\r\n                            <option>25</option>\r\n                            <option>50</option>\r\n                            <option>75</option>\r\n                        </select>\r\n                    </div>\r\n                </form>\r\n                <table class=\"table table-bordered table-striped\">\r\n                    <thead>\r\n                    <tr>\r\n                        <th>Фото</th>\r\n                        <th class=\"st-sort-disable\">Название</th>\r\n                        <th class=\"st-sort-disable\">Категория</th>\r\n                        <th></th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr grid-item>\r\n                        <td><img ng-if=\"item.image.file\" src=\"{{publicUrl+item.image.dir+'thumb_'+item.image.file}}\" /></td>\r\n                        <td width=\"30%\" ng-bind=\"item.name\"></td>\r\n                        <td width=\"30%\" ng-bind=\"item.category.name\"></td>\r\n                        <td>\r\n                            <button ui-sref=\"site({id:item.id})\" class=\"btn btn-primary\"><i\r\n                                    class=\"fa fa-pencil\"></i></button>\r\n                            <button class=\"btn btn-danger\" ng-click=\"delete(item.id)\"><i class=\"fa fa-times\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n\r\n                <form class=\"form-inline pull-right margin-bottom-basic\">\r\n                    <div class=\"form-group\">\r\n                        <grid-pagination max-size=\"15\"\r\n                                         boundary-links=\"true\"\r\n                                         class=\"pagination-sm\"\r\n                                         total-items=\"paginationOptions.totalItems\"\r\n                                         ng-model=\"paginationOptions.currentPage\"\r\n                                         ng-change=\"reloadGrid()\"\r\n                                         items-per-page=\"paginationOptions.itemsPerPage\"></grid-pagination>\r\n                    </div>\r\n                    <div class=\"form-group items-per-page\">\r\n                        <label for=\"itemsOnPageSelect2\">Items per page:</label>\r\n                        <select id=\"itemsOnPageSelect2\" class=\"form-control input-sm\"\r\n                                ng-init=\"paginationOptions.itemsPerPage = '10'\"\r\n                                ng-model=\"paginationOptions.itemsPerPage\" ng-change=\"reloadGrid()\">\r\n                            <option>10</option>\r\n                            <option>25</option>\r\n                            <option>50</option>\r\n                            <option>75</option>\r\n                        </select>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"form-group animate bounceInLeft text-right col-xs-12\">\r\n                <button ui-sref=\"site\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i>\r\n                    Добавить\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 21 */,
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('SiteController', SiteController);
  function SiteController($scope, $log, $http, RestService, $state, toastr) {

    $scope.id = $state.params.id;
    $scope.model = {};
    $scope.sectionModel = {};
    $scope.formAddSectionIsOpen = false;
    $scope.isLoaded = false;
    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.saveSection = function () {
      if ($scope.sectionModel.name) {
        $scope.sectionModel.site = $scope.id;
        RestService.post('section/' + $scope.model.id, { data: $scope.sectionModel }).then(function (data) {
          RestService.get('section/' + $scope.model.id).then(function (listResponse) {
            $scope.gridOptions.data = listResponse.data.data;
            $scope.formAddSectionIsOpen = false;
            $scope.sectionModel = {};
            toastr.success('Раздел успешно содан');
          }).catch(function (error) {
            toastr.error(error);
            console.log('error', error);
          });
        }).catch(function (error) {
          toastr.error(error);
          console.log('error', error);
        });
      }
    };

    $scope.saveSite = function () {
      $scope.isLoaded = false;
      if ($scope.model.id) {
        RestService.put('site/' + $scope.model.id, { data: $scope.model }).then(function (response) {
          if (!response.data.error) {
            console.log('response', response);
            toastr.success('Запись успешно обновленна');
          } else {
            console.log('error', response.data.error);
            toastr.error(response.data.error.message);
          }
          $scope.isLoaded = true;
        }).catch(function (error) {
          console.log('error', error);
          toastr.error('Ошибка обновления');
          $scope.isLoaded = true;
          throw error;
        });
      } else {
        RestService.post('site', { data: $scope.model }).then(function (response) {
          if (!response.data.error) {
            toastr.success('Запись успешно создана');
          } else {
            console.log('error', response.data.error);
            toastr.error(response.data.error.message);
          }
          $scope.isLoaded = true;
        }).catch(function (error) {
          console.log('error', error);
          toastr.success('Ошибка создания');
          $scope.isLoaded = true;
          throw error;
        });
      }
    };

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('section/' + id).then(function (response) {

          if (response.data.status === 'deleted') {
            toastr.error('Продукция успешно удаленна');
            loadData();
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    function loadData() {
      if ($scope.id) {
        RestService.get('site/' + $scope.id).then(function (response) {
          $scope.isLoaded = true;
          $('table.table').show(400);
          $scope.model = response.data.data;

          RestService.get('section/' + $scope.id).then(function (responseSection) {
            $scope.gridOptions.data = responseSection.data.data;
          });
        });
      } else {
        $scope.isLoaded = true;
      }
    }
  };
})();

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"panel panel-info\">\r\n                <div class=\"panel-heading\"><h3 class=\"panel-title\">Сайт</h3></div>\r\n                <div class=\"panel-body\">\r\n                    <form class=\"animate fadeInDown product-form\">\r\n                        <div class=\"panel-body\" id=\"according01\">\r\n                            <div class=\"form-group\">\r\n                                <label for=\"name\">Название:</label>\r\n                                <input type=\"text\" class=\"form-control\" required id=\"name\"\r\n                                       ng-model=\"model.name\">\r\n                            </div>\r\n                        </div>\r\n                        <input type=\"hidden\" id=\"id\" ng-model=\"post.id\">\r\n                        <div class=\"form-group animate bounceInLeft\">\r\n                            <button class=\"btn btn-info\" ui-sref=\"sites\">Назад</button>&nbsp;\r\n                            <button class=\"btn btn-success\" ng-click=\"saveSite()\">Сохранить</button>\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n            <div class=\"panel panel-default\">\r\n                <div class=\"panel-heading\"><h3 class=\"panel-title\">Разделы</h3></div>\r\n                <div class=\"panel-body\">\r\n\r\n                    <div class=\"form-group animate bounceInLeft text-right\">\r\n                        <button ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\" class=\"btn btn-success btn-sm\">\r\n                            <i\r\n                                    class=\"fa fa-plus\"></i>\r\n                            Добавить раздел\r\n                        </button>\r\n                    </div>\r\n                    <br/>\r\n                    <div ng-if=\"formAddSectionIsOpen\">\r\n                        <form class=\"animate fadeInDown product-form\">\r\n                            <div class=\"panel-group\">\r\n                                <div class=\"panel panel-default\">\r\n                                    <div class=\"panel-body\">\r\n                                        <div class=\"form-group\">\r\n                                            <label for=\"name\">Название:</label>\r\n                                            <input type=\"text\" class=\"form-control\" required id=\"sectionName\"\r\n                                                   ng-model=\"sectionModel.name\">\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"form-group animate bounceInLeft\">\r\n                                <button type=\"button\" class=\"btn btn-info\"\r\n                                        ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\">Скрыть\r\n                                </button>&nbsp;\r\n                                <button class=\"btn btn-success\" ng-click=\"saveSection()\">Сохранить</button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n                    <div ng-if=\"gridOptions.data.length\">\r\n                        <div grid-data id='grid1' grid-options=\"gridOptions\" grid-actions=\"gridActions\">\r\n                            <table class=\"table table-bordered table-striped\">\r\n                                <thead>\r\n                                <tr>\r\n                                    <th class=\"st-sort-disable\">Название</th>\r\n                                    <th></th>\r\n                                </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                <tr grid-item>\r\n                                    <td width=\"80%\"><a ui-sref=\"section({id:id, sid:item.id})\" ng-bind=\"item.name\"></a></td>\r\n                                    <td class=\"text-center\">\r\n                                        <button ui-sref=\"section({id:model.id, sid: item.id})\" class=\"btn btn-primary\">\r\n                                            <i\r\n                                                    class=\"fa fa-pencil\"></i></button>\r\n                                        <button class=\"btn btn-danger\" ng-click=\"delete(item.id)\"><i\r\n                                                class=\"fa fa-times\"></i>\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                        <br/>\r\n                        <div class=\"form-group animate bounceInLeft text-right\">\r\n                            <button ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\"\r\n                                    class=\"btn btn-success btn-sm\"><i\r\n                                    class=\"fa fa-plus\"></i>\r\n                                Добавить раздел\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div ng-if=\"!gridOptions.data.length\">\r\n                <div class=\"text-center\"><i>Список разделов пуст</i></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('SectionController', SectionController);
  function SectionController($scope, $log, $http, RestService, $state, toastr) {

    $scope.siteId = $state.params.id;
    $scope.sid = $state.params.sid;
    $scope.model = {};
    $scope.keyModel = {};
    $scope.subModel = {};
    $scope.formAddSectionIsOpen = false;
    $scope.formAddKeywordTypeOne = true;
    $scope.formAddSubSectionIsOpen = false;
    $scope.showSubSection = false;
    $scope.showKeyWord = false;
    $scope.editformSectionIsOpen = false;
    $scope.isLoaded = false;
    $scope.gridOptions = {
      data: [],
      sort: { predicate: 'count',
        direction: 'asc' },
      urlSync: true
    };

    $scope.gridSubOptions = {
      data: [],
      sort: {},
      urlSync: true
    };
    loadData();

    $scope.changeFormAddKeywordTypeOne = function (isOne) {
      $scope.formAddKeywordTypeOne = isOne;
    };

    $scope.saveKey = function () {

      // Одиночная вставка
      if ($scope.formAddKeywordTypeOne) {
        if ($scope.keyModel.name) {
          $scope.keyModel.site = $scope.siteId;
          RestService.post('keyword/' + $scope.siteId + '/' + $scope.model.id, { data: $scope.keyModel }).then(function (data) {
            if (data.data.status === 'ok') {
              RestService.get('keyword/' + $scope.model.id).then(function (listResponse) {
                $scope.gridOptions.data = listResponse.data.data;
                $scope.formAddSectionIsOpen = false;
                $scope.keyModel = {};
                toastr.success('Ключ. слово успешно содан');
              }).catch(function (error) {
                toastr.error(error);
                console.log('error', error);
              });
            } else {
              toastr.error(data.data.error);
              console.log('error', data.data.error);
            }
          }).catch(function (error) {
            toastr.error(error);
            console.log('error', error);
          });
        } else {
          toastr.error('Не заполненный ключевые слова');
        }
      } else {
        if ($scope.keyModel.list) {

          var text = $scope.keyModel.list;
          var lines = text.split('\n');
          var params = [];
          var result = [];
          var value = '';
          for (var i = 0; i < lines.length; i++) {
            params = lines[i].split('\t');
            params[1] = params[1].replace(/([ ])*/g, '');
            console.log(params[0], ' - ', parseInt(params[1]));
            result.push({ name: params[0], value: parseInt(params[1]) });
          }

          RestService.post('keyword-list/' + $scope.siteId + '/' + $scope.model.id, { data: result }).then(function (data) {
            console.log('data', data);
            if (data.data.countAdd > 0) {
              toastr.success('Успешно внесенно ' + data.data.countAdd);
              $scope.keyModel.list = '';
              $scope.formAddSectionIsOpen = false;

              RestService.get('keyword/' + $scope.sid).then(function (responseSection) {
                $scope.gridOptions.data = responseSection.data.data;
              });
            }
            if (data.data.countAdd < result.length) {
              toastr.error('Не внесенны ' + (result.length - data.data.countAdd));
              toastr.error(data.data.errors);
            }
          }).catch(function (error) {
            toastr.error(error);
          });
        } else {
          toastr.error('Не заполненный ключевые слова');
        }
      }
    };

    $scope.saveSubSection = function () {
      $scope.isLoaded = false;
      RestService.post('sub-section/' + $scope.siteId + '/' + $scope.model.id, { data: $scope.subModel }).then(function (response) {
        if (!response.data.error) {
          console.log('response', response);
          toastr.success('Запись успешно создана');

          RestService.get('sub-section/' + $scope.model.id).then(function (responseSubSection) {
            $scope.subModel = {};
            $scope.formAddSubSectionIsOpen = false;
            $scope.gridSubOptions.data = responseSubSection.data.list;
          });
        } else {
          console.log('error', response.data.error);
          toastr.error(response.data.error.message);
        }
        $scope.isLoaded = true;
      }).catch(function (error) {
        console.log('error', error);
        toastr.error('Ошибка обновления');
        $scope.isLoaded = true;
        throw error;
      });
    };

    $scope.saveSection = function () {
      $scope.isLoaded = false;
      RestService.put('section/' + $scope.model.id, { data: $scope.model }).then(function (response) {
        if (!response.data.error) {
          console.log('response', response);
          toastr.success('Запись успешно обновленна');
        } else {
          console.log('error', response.data.error);
          toastr.error(response.data.error.message);
        }
        $scope.isLoaded = true;
      }).catch(function (error) {
        console.log('error', error);
        toastr.error('Ошибка обновления');
        $scope.isLoaded = true;
        throw error;
      });
    };

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('keyword/' + id).then(function (response) {

          if (response.data.status === 'deleted') {

            RestService.get('keyword/' + $scope.sid).then(function (responseSection) {
              $scope.gridOptions.data = responseSection.data.data;
              toastr.success('Запись успешно удаленна');
            }).catch(function (error) {
              toastr.error('Ошибка удаление ключ. слова');
              toastr.error(error);
            });
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    $scope.deleteSection = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('section/' + id).then(function (response) {

          if (response.data.status === 'deleted') {

            RestService.get('section/' + $scope.sid).then(function (responseSection) {
              $scope.gridSubOptions.data = responseSection.data.data;
              toastr.success('Запись успешно удаленна');
            }).catch(function (error) {
              toastr.error('Ошибка удаление');
              toastr.error(error);
            });
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    function loadData() {
      if ($scope.siteId) {
        RestService.get('section/' + $scope.siteId + '/' + $scope.sid).then(function (response) {
          $scope.isLoaded = true;
          $('table.table').show(400);
          $scope.model = response.data.data;

          RestService.get('keyword/' + $scope.sid).then(function (responseSection) {
            $scope.gridOptions.data = responseSection.data.data;
            if ($scope.gridOptions.data.length) {
              $scope.showKeyWord = true;
            }

            RestService.get('sub-section/' + $scope.sid).then(function (responseSubSection) {
              $scope.gridSubOptions.data = responseSubSection.data.list;
              if ($scope.gridSubOptions.data.length) {
                $scope.showSubSection = true;
              }
            });
          });
        });
      } else {
        $state.go('sites');
      }
    }
  }
})();

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <p>\r\n                <a ui-sref=\"site({id:siteId})\"><i class=\"fa fa-chevron-left\"></i> Назад</a>\r\n            </p>\r\n            <div class=\"panel panel-info\">\r\n                <div class=\"panel-heading\"><h3 class=\"panel-title\">Раздел</h3></div>\r\n                <div class=\"panel-body\">\r\n                    <div ng-show=\"!editformSectionIsOpen\">\r\n                        <div class=\"form-group\">\r\n                            <label for=\"name\">Название:</label>\r\n                            <br/>\r\n                            {{model.name}}&nbsp;<a ng-click=\"editformSectionIsOpen = !editformSectionIsOpen\"><i\r\n                                class=\"fa fa-edit\"></i> </a>\r\n                        </div>\r\n                    </div>\r\n                    <form ng-show=\"editformSectionIsOpen\" class=\"product-form\">\r\n                        <div class=\"panel-group\">\r\n                            <div class=\"panel panel-default\">\r\n                                <div class=\"panel-body\" id=\"according01\">\r\n                                    <div class=\"form-group\">\r\n                                        <label for=\"name\">Название:</label>\r\n                                        <input type=\"text\" class=\"form-control\" required id=\"name\"\r\n                                               ng-model=\"model.name\">\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <input type=\"hidden\" id=\"id\" ng-model=\"model.id\">\r\n                        </div>\r\n                        <div class=\"form-group animate bounceInLeft\">\r\n                            <button class=\"btn btn-info\" ng-click=\"editformSectionIsOpen = !editformSectionIsOpen\">\r\n                                Скрыть\r\n                            </button>&nbsp;\r\n                            <button class=\"btn btn-success\" ng-click=\"saveSection()\">Сохранить раздел</button>\r\n                        </div>\r\n                    </form>\r\n                    <div class=\"text-right\">\r\n                        <a class=\"btn btn-default\" ng-click=\"showKeyWord = !showKeyWord\">\r\n                            <i class=\"fa fa-tasks\"></i>&nbsp;\r\n                            Ключевые слова </a>\r\n                        <a class=\"btn btn-default\" ng-click=\"showSubSection = !showSubSection\">\r\n                            <i class=\"fa fa-folder\"></i>&nbsp;Вложенные разделы </a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div ng-show=\"showSubSection\" class=\"panel panel-default\">\r\n                <div class=\"panel-heading\"><h3 class=\"panel-title\">Вложенные разделы</h3></div>\r\n                <div class=\"panel-body\">\r\n                    <div class=\"form-group animate bounceInLeft text-right\">\r\n                        <button ng-click=\"formAddSubSectionIsOpen = !formAddSubSectionIsOpen\"\r\n                                class=\"btn btn-success btn-sm\">\r\n                            <i lass=\"fa fa-plus\"></i>Добавить раздел\r\n                        </button>\r\n                    </div>\r\n                    <br/>\r\n                    <div ng-show=\"formAddSubSectionIsOpen\">\r\n                        <form class=\"animate fadeInDown product-form\">\r\n                            <div class=\"panel-group\">\r\n                                <div class=\"panel panel-default\">\r\n                                    <div class=\"panel-body\">\r\n                                        <div class=\"form-group\">\r\n                                            <label for=\"name\">Название:</label>\r\n                                            <input type=\"text\" class=\"form-control\" required\r\n                                                   ng-model=\"subModel.name\">\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"form-group animate bounceInLeft\">\r\n                                <button type=\"button\" class=\"btn btn-info\"\r\n                                        ng-click=\"formAddSubSectionIsOpen = !formAddSubSectionIsOpen\">Скрыть\r\n                                </button>&nbsp;\r\n                                <button class=\"btn btn-success\" ng-click=\"saveSubSection()\">Сохранить</button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n                    <div ng-if=\"gridSubOptions.data.length\">\r\n                        <div grid-data id='grid2' grid-options=\"gridSubOptions\" grid-actions=\"gridActions\">\r\n                            <table class=\"table table-bordered table-striped\">\r\n                                <thead>\r\n                                <tr>\r\n                                    <th class=\"st-sort-disable\">Название</th>\r\n                                    <th class=\"st-sort-disable\">Ключ. слов</th>\r\n                                    <th></th>\r\n                                </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                <tr grid-item>\r\n                                    <td width=\"60%\"><a ui-sref=\"section({id:siteId,sid:item.id})\"\r\n                                                       ng-bind=\"item.name\"></a></td>\r\n                                    <td width=\"20%\" ng-bind=\"item.count\"></td>\r\n                                    <td class=\"text-center\">\r\n                                        <button ui-sref=\"keyword({id:model.id})\" class=\"btn btn-primary\"><i\r\n                                                class=\"fa fa-pencil\"></i></button>\r\n                                        <button class=\"btn btn-danger\" ng-click=\"deleteSection(item.id)\"><i\r\n                                                class=\"fa fa-times\"></i>\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                        <br/>\r\n                    </div>\r\n                    <div ng-if=\"!gridSubOptions.data.length\">\r\n                        <div class=\"text-center\"><i>Под разделов нет</i></div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div ng-show=\"showKeyWord\" class=\"panel panel-default\">\r\n                <div class=\"panel-heading\"><h3 class=\"panel-title\">Ключевые слова</h3></div>\r\n                <div class=\"panel-body\">\r\n                    <div class=\"form-group animate bounceInLeft text-right\">\r\n                        <button ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\" class=\"btn btn-success btn-sm\">\r\n                            <i\r\n                                    class=\"fa fa-plus\"></i>\r\n                            Добавить ключевое слово\r\n                        </button>\r\n                    </div>\r\n                    <br/>\r\n                    <div ng-if=\"formAddSectionIsOpen\">\r\n                        <form class=\"animate fadeInDown product-form\">\r\n                            <div class=\"panel-group\">\r\n                                <div class=\"panel panel-default\">\r\n                                    <div class=\"panel-body\">\r\n                                        <div class=\"\" role=\"tabpanel\" data-example-id=\"togglable-tabs\">\r\n                                            <ul id=\"myTab\" class=\"nav nav-tabs bar_tabs\" role=\"tablist\">\r\n                                                <li role=\"presentation\" class=\"active\">\r\n                                                    <a id=\"one-tab\" role=\"tab\"\r\n                                                       ng-click=\"changeFormAddKeywordTypeOne(true)\" data-toggle=\"tab\"\r\n                                                       aria-expanded=\"true\">Одиночная вставка</a>\r\n                                                </li>\r\n                                                <li role=\"presentation\" class=\"\">\r\n                                                    <a role=\"many-tab\" id=\"profile-tab\"\r\n                                                       data-toggle=\"tab\" ng-click=\"changeFormAddKeywordTypeOne(false)\"\r\n                                                       aria-expanded=\"false\">Групповая вставка</a>\r\n                                                </li>\r\n                                            </ul>\r\n                                            <div id=\"myTabContent\" class=\"tab-content\">\r\n                                                <div ng-if=\"formAddKeywordTypeOne\" role=\"tabpanel\"\r\n                                                     class=\"tab-pane active in\" id=\"tab_content1\"\r\n                                                     aria-labelledby=\"one-tab\">\r\n                                                    <div class=\"form-group col-xs-6\">\r\n                                                        <label for=\"name\">Название:</label>\r\n                                                        <input type=\"text\" class=\"form-control\" required\r\n                                                               ng-model=\"keyModel.name\">\r\n                                                    </div>\r\n                                                    <div class=\"form-group col-xs-6\">\r\n                                                        <label for=\"name\">Значение:</label>\r\n                                                        <input type=\"text\" class=\"form-control\" required\r\n                                                               ng-model=\"keyModel.value\">\r\n                                                    </div>\r\n                                                </div>\r\n                                                <div ng-if=\"!formAddKeywordTypeOne\" role=\"tabpanel\"\r\n                                                     class=\"tab-pane active in\" id=\"tab_content2\"\r\n                                                     aria-labelledby=\"many-tab\">\r\n                                                    <div class=\"form-group col-xs-12\">\r\n                                                        <label for=\"name\"><label for=\"text\">Значение:</label></label>\r\n                                                        <textarea id=\"text\" class=\"form-control col-xs-12\"\r\n                                                                  style=\"min-height: 100px\" required\r\n                                                                  ng-model=\"keyModel.list\"></textarea>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"form-group animate bounceInLeft\">\r\n                                <button type=\"button\" class=\"btn btn-info\"\r\n                                        ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\">Скрыть\r\n                                </button>&nbsp;\r\n                                <button class=\"btn btn-success\" ng-click=\"saveKey()\">Сохранить</button>\r\n                            </div>\r\n                        </form>\r\n                    </div>\r\n\r\n                    <div grid-data id='grid1' grid-options=\"gridOptions\" grid-actions=\"gridActions\">\r\n                        <div ng-if=\"gridOptions.data.length\">\r\n                            <div class=\"col-md-9 text-right\">\r\n                                <form class=\"form-inline pull-right margin-bottom-basic\">\r\n                                    <div class=\"form-group\">\r\n                                        <grid-pagination max-size=\"5\"\r\n                                                         boundary-links=\"true\"\r\n                                                         class=\"pagination-sm\"\r\n                                                         total-items=\"paginationOptions.totalItems\"\r\n                                                         ng-model=\"paginationOptions.currentPage\"\r\n                                                         ng-change=\"reloadGrid()\"\r\n                                                         items-per-page=\"paginationOptions.itemsPerPage\"></grid-pagination>\r\n                                    </div>\r\n                                    <div class=\"form-group items-per-page\">\r\n                                        <label for=\"itemsOnPageSelect1\">Items per page:</label>\r\n                                        <select id=\"itemsOnPageSelect1\" class=\"form-control input-sm\"\r\n                                                ng-init=\"paginationOptions.itemsPerPage = '100'\"\r\n                                                ng-model=\"paginationOptions.itemsPerPage\" ng-change=\"reloadGrid()\">\r\n                                            <option>10</option>\r\n                                            <option>25</option>\r\n                                            <option>50</option>\r\n                                            <option>100</option>\r\n                                            <option>200</option>\r\n                                        </select>\r\n                                    </div>\r\n                                </form>\r\n                            </div>\r\n                        </div>\r\n                        <table class=\"table table-bordered table-striped\">\r\n                            <thead>\r\n                            <tr>\r\n                                <th class=\"st-sort-disable\"></th>\r\n                                <th class=\"st-sort-disable\" sortable=\"name\" class=\"sortable\">Название</th>\r\n                                <th class=\"st-sort-disable\" sortable=\"count\" class=\"sortable\">Значение</th>\r\n                                <th></th>\r\n                            </tr>\r\n                            </thead>\r\n                            <tbody>\r\n                            <tr grid-item>\r\n                                <td width=\"5%\" ng-bind=\"$index + 1\"></td>\r\n                                <td width=\"55%\" ng-bind=\"item.name\"></td>\r\n                                <td width=\"20%\" class=\"text-right\" ng-bind=\"item.value\"></td>\r\n                                <td class=\"text-center\">\r\n                                    <button class=\"btn btn-danger\" ng-click=\"delete(item.id)\"><i\r\n                                            class=\"fa fa-times\"></i>\r\n                                    </button>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                    <br/>\r\n                    <div class=\"form-group animate bounceInLeft text-right\">\r\n                        <button ng-click=\"formAddSectionIsOpen = !formAddSectionIsOpen\"\r\n                                class=\"btn btn-success btn-sm\">\r\n                            <i class=\"fa fa-plus\"></i>\r\n                            Добавить ключевое слово\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div ng-if=\"!gridOptions.data.length\">\r\n                    <div class=\"text-center\"><i>Список ключевых слов пуст</i></div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ })
/******/ ]);