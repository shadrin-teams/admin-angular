angular.module('mainApp', [
  'ui.router',
  'dataGrid',
  'pagination',
  'angularFileUpload',
  'ngAnimate',
  'toastr',
  'froala',
  'ngQuickDate'])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'app/views/index.html',
        controller: 'IndexController'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/views/login.html',
        controller: 'LoginController'
      })
      .state('sites', {
        url: '/sites',
        templateUrl: 'app/views/sites.html',
        controller: 'SitesController',
        controllerAs: 'sitesCtrl'
      })
      .state('site', {
        url: '/site/:id',
        templateUrl: 'app/views/site.html',
        controller: 'SiteController',
        controllerAs: 'siteCtrl'
      })
      .state('section', {
        url: '/section/:id/:sid',
        templateUrl: 'app/views/section.html',
        controller: 'SectionController',
        controllerAs: '$ctrl'
      });

    $urlRouterProvider.otherwise('/login');
  })

  .run(function($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
    $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState) {
      if (!AuthService.isAuthenticated()) {
        if (next.name !== 'login' && next.name !== 'register') {
          event.preventDefault();
          $state.go('login');
        }
      }
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        if (!angular.isString(error)) {
          error = JSON.stringify(error);
        }
        $log.error('$stateChangeError: ' + error);
      }
    );

  });

require('./load');