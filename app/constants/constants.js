angular.module('mainApp')

  .constant('AUTH_EVENTS', {
    notAuthenticated: 'auth-not-authenticated'
  })

  .constant('API_PUBLIC', {
    url: 'http://localhost:3001/'
  })

  .constant('API_ENDPOINT', {
    url: 'http://localhost:3001/api' //188.120.241.78
  });