(function() {

    'use strict';

    angular
        .module('mainApp')

        /**
         * AppController
         * Description: Sets up a controller
         */
        .controller('LoginController', LoginController);
        function LoginController($scope, AuthService, $rootScope, $state) {
            $scope.user = {
                name: '',
                password: ''
            };

            $scope.login = function() {
                AuthService.login($scope.user).then(function(msg) {
                    $rootScope.$broadcast('login', {user:$scope.user});
                    $state.go('index');
                }, function(errMsg) {
                    alert('Login failed!'+errMsg);
                });
            };
        };

})();