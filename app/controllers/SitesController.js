(function() {

  'use strict';

  angular
    .module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('SitesController', SitesController);
  function SitesController($scope, $log, $http, RestService, API_PUBLIC, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.$watch(function() {
      return $state.params.cat
    }, function(newVal, oldVal) {
      $log.log('$watch $state', newVal);
    });

    $scope.listSite = [];
    $scope.cat = $state.params.cat;
    $scope.name = 'Посты';
    $scope.find = {};
    $scope.gridActions = {};
    $scope.publicUrl = API_PUBLIC.url;

    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('site/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {
              toastr.error('Продукция успешно удаленна');
              loadData();
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    function loadData() {
      RestService.get('site', {})
        .then(function(response) {
          $('table.table').show(400);
          $scope.listSite = response.data.data;
          $scope.gridOptions.data = response.data.data;
          console.log('get products', response);
        })
    };
  };
})();