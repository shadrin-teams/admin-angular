//ngQuickDate https://github.com/adamalbrecht/ngQuickDate
require('../bower_components/ng-quick-date/dist/ng-quick-date.min');

// Controllers
require('./controllers/AppController');
require('./controllers/AppController');
require('./controllers/AppController');
require('./controllers/IndexController');
require('./controllers/LoginController');
require('./controllers/SitesController');
require('./controllers/SiteController');
require('./controllers/SectionController');

// Views
require('./views/editor.html');
require('./views/index.html');
require('./views/login.html');
require('./views/menu.html');
require('./views/sites.html');
require('./views/site.html');
require('./views/section.html');

// Services
require('./services/services');
require('./services/rest');

// Constants
require('./constants/constants');
