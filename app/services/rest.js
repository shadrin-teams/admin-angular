angular.module('mainApp')

    .service('RestService', function($q, $http, API_ENDPOINT) {

        function send(method, url, data){

            return $http({
                method: method,
                url: API_ENDPOINT.url+'/'+url,
                data: data
            });
        }

        function get(url, data) {
            return send('GET', url, data);
        }

        function put(url, data) {
            return send('PUT', url, data);
        }

        function post(url, data) {
            return send('POST', url, data);
        }

        function del(url, data) {
            return send('DELETE', url, data);
        }

        return {
            post: post,
            put: put,
            get: get,
            delete: del
        };
    })
