angular.module('mainApp')

    .service('AuthService', function($q, $http, API_ENDPOINT) {
        let LOCAL_TOKEN_KEY = 'token';
        let authenticated = false;
        let authToken;

        function loadUserCredentials() {
            let token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (token) {
                useCredentials(token);
                console.log("loadUserCredentials");
            }
        }

        function storeUserCredentials(token) {
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            useCredentials(token);
        }

        function useCredentials(token) {
            authenticated = true;
            authToken = token;
            console.log( 'authenticated', authenticated );

            // Set the token as header for your requests!
            $http.defaults.headers.common.Authorization = 'JWT '+authToken;
        }

        function destroyUserCredentials() {
            authToken = undefined;
            authenticated = false;
            $http.defaults.headers.common.Authorization = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
        }

        let register = function(user) {
            return $q(function(resolve, reject) {
                $http.post(API_ENDPOINT.url + '/signup', user).then(function(result) {
                    if (result.data.success) {
                        resolve(result.data.msg);
                    } else {
                        reject(result.data.msg);
                    }
                });
            });
        };

        let login = function(user) {
            return $q(function(resolve, reject) {
                $http.post(API_ENDPOINT.url + '/auth', {email:user.email,password:user.password}).then(function(result) {
                    if (result.data.success) {

                        storeUserCredentials(result.data.token);
                        resolve(result.data.msg);
                    } else {
                        reject(result.data.msg);
                    }
                });
            });
        };

        let logout = function() {
            destroyUserCredentials();
        };

        loadUserCredentials();

        return {
            login: login,
            register: register,
            logout: logout,
            isAuthenticated: function() {return authenticated;},
        };
    })

    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })

    .config(function ($httpProvider) {
        console.log('httpProvider', $httpProvider.interceptors);
        $httpProvider.interceptors.push('AuthInterceptor');
    });